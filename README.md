# vagrant-provision-docker

I use a simple vagrant provisioning script for all my Docker projects.
Instead of keeping a copy of this Vagrant provisioner in every project, I just
store it in `teh interwebs` and make a curl call when it's needed.

If you have a Docker project, reusing this script can minimize the amount of
meta crap you have to track across your projects.


## Minimum Requirements

### Written for Ubuntu boxes

I currently use [`ubuntu/xenial64`](https://vagrantcloud.com/ubuntu/bionic64).

### Installation location must be able to make remote URL requests

Vagrant provides `curl` by default, so you just need to make sure your network
allows the external connection to GitHub. If that's not possible, grab the code
and maintain it in your repo.


## Installation

### Option 1 - Standard remote call

In your `Vagrantfile`...

```ruby
Vagrant.configure("2") do |config|
  # ...

  # Docker vagrant box by BattleBrisket
  # https://gitlab.com/BattleBrisket/vagrant-provision-docker
  config.vm.provision "shell", inline: "curl -fsS https://gitlab.com/BattleBrisket/vagrant-provision-docker/-/raw/master/script | bash"
end
```

### Option 2 - Local repo fork

First, clone this repository inside your project. Release versions are not
currently maintained, so you can just clone master.

Then adjust the `config.vm.provision` call in your `Vagrantfile`...

```ruby
Vagrant.configure("2") do |config|
  # ...

  # Docker vagrant box by BattleBrisket
  # https://gitlab.com/BattleBrisket/vagrant-provision-docker
  config.vm.provision "shell", path: "/vagrant-provision-docker/script"
end
```

**Note:** The above example assumes you cloned the repository to a directory named
`vagrant-provision-docker`, located in the same directory as your `Vagrantfile`.

## Sample Vagrantfile

If the above instructions aren't making sense, or if you are new to Vagrant,
I've included a sample Vagrantfile ([browse] | [raw]).
It includes a call to the provisioner script, so you'll see where/how to add it
to your own Vagrantfile.

It also includes a set of handy Vagrant config tweaks that make it into nearly
all my projects.

## Bug Reports

[Drop us a line in the issues](https://gitlab.com/BattleBrisket/vagrant-provision-docker/-/issues).

**Be sure to include sample code that reproduces the problem.**

## License

Copyright (c) Frank Koehl. It is free software, and may be redistributed under the terms specified in the LICENSE file.

[browse]: https://gitlab.com/BattleBrisket/vagrant-provision-docker/-/blob/master/Vagrantfile-sample
[raw]: https://gitlab.com/BattleBrisket/vagrant-provision-docker/-/raw/master/Vagrantfile-sample
