# -*- mode: ruby -*-
# vi: set ft=ruby :

# Server name
NAME   = "BOXNAME"
# Fully qualified domain name (default is usually fine for development)
FQDN   = "#{NAME}.example.com"
# Name of the vagrant box image
BOX    = "ubuntu/bionic64"
# Memory to allocate from host machine (in KB)
MEMORY = 4096
# Number of CPU cores
CPUS   = 4
# Ports to be forwarded (host => guest)
PORTS  = {
#  80 => 80,
#  443 => 443,
}

Vagrant.configure("2") do |config|
  config.vm.box = BOX
  config.vm.hostname = FQDN

  # Use the normal insecure key
  # https://github.com/hashicorp/vagrant/issues/2608
  config.ssh.insert_key = false

  PORTS.each do |host, guest|
    config.vm.network :forwarded_port, host: host, guest: guest
  end

  config.vm.provider :virtualbox do |vb|
    vb.customize ["modifyvm", :id,
      # Basics.
      "--name",       NAME,
      "--memory",     MEMORY,
      # I/O APIC must be enabled to support a multi-core guest.
      "--cpus",       CPUS,
      "--ioapic",     "on",
      # Enable native host virtualization features (yields better performance).
      "--pae",        "on",
      "--hwvirtex",   "on",
      "--largepages", "on",
      "--vtxvpid",    "on",
      # This causes the virtual machine to proxy DNS requests through the host
      # via NAT. Without this, the default resolver on the guest will introduce
      # a 5 second latency on every HTTP request... which is a real downer.
      "--natdnshostresolver1", "on"
    ]
  end

  config.vm.hostname = FQDN

  # Docker vagrant box by BattleBrisket
  # https://gitlab.com/BattleBrisket/vagrant-provision-docker
  config.vm.provision "shell", inline: "curl -fsS https://gitlab.com/BattleBrisket/vagrant-provision-docker/-/raw/master/script | bash"

end

#
# DONT FORGET!
#
# Force update VirtualBox Guest Additions
# Run the following command inside same directory as Vagrantfile
# Must be done once on your dev system
#
#   vagrant plugin install vagrant-vbguest
#

#
# Vagrant's default insecure private key
# You can use this to SSH into the vagrant box
# if the `vagrant ssh` command doesn't work
#
# DON'T copy/paste the `=begin` and `=end`
#
=begin
-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEA6NF8iallvQVp22WDkTkyrtvp9eWW6A8YVr+kz4TjGYe7gHzI
w+niNltGEFHzD8+v1I2YJ6oXevct1YeS0o9HZyN1Q9qgCgzUFtdOKLv6IedplqoP
kcmF0aYet2PkEDo3MlTBckFXPITAMzF8dJSIFo9D8HfdOV0IAdx4O7PtixWKn5y2
hMNG0zQPyUecp4pzC6kivAIhyfHilFR61RGL+GPXQ2MWZWFYbAGjyiYJnAmCP3NO
Td0jMZEnDkbUvxhMmBYSdETk1rRgm+R4LOzFUGaHqHDLKLX+FIPKcF96hrucXzcW
yLbIbEgE98OHlnVYCzRdK8jlqm8tehUc9c9WhQIBIwKCAQEA4iqWPJXtzZA68mKd
ELs4jJsdyky+ewdZeNds5tjcnHU5zUYE25K+ffJED9qUWICcLZDc81TGWjHyAqD1
Bw7XpgUwFgeUJwUlzQurAv+/ySnxiwuaGJfhFM1CaQHzfXphgVml+fZUvnJUTvzf
TK2Lg6EdbUE9TarUlBf/xPfuEhMSlIE5keb/Zz3/LUlRg8yDqz5w+QWVJ4utnKnK
iqwZN0mwpwU7YSyJhlT4YV1F3n4YjLswM5wJs2oqm0jssQu/BT0tyEXNDYBLEF4A
sClaWuSJ2kjq7KhrrYXzagqhnSei9ODYFShJu8UWVec3Ihb5ZXlzO6vdNQ1J9Xsf
4m+2ywKBgQD6qFxx/Rv9CNN96l/4rb14HKirC2o/orApiHmHDsURs5rUKDx0f9iP
cXN7S1uePXuJRK/5hsubaOCx3Owd2u9gD6Oq0CsMkE4CUSiJcYrMANtx54cGH7Rk
EjFZxK8xAv1ldELEyxrFqkbE4BKd8QOt414qjvTGyAK+OLD3M2QdCQKBgQDtx8pN
CAxR7yhHbIWT1AH66+XWN8bXq7l3RO/ukeaci98JfkbkxURZhtxV/HHuvUhnPLdX
3TwygPBYZFNo4pzVEhzWoTtnEtrFueKxyc3+LjZpuo+mBlQ6ORtfgkr9gBVphXZG
YEzkCD3lVdl8L4cw9BVpKrJCs1c5taGjDgdInQKBgHm/fVvv96bJxc9x1tffXAcj
3OVdUN0UgXNCSaf/3A/phbeBQe9xS+3mpc4r6qvx+iy69mNBeNZ0xOitIjpjBo2+
dBEjSBwLk5q5tJqHmy/jKMJL4n9ROlx93XS+njxgibTvU6Fp9w+NOFD/HvxB3Tcz
6+jJF85D5BNAG3DBMKBjAoGBAOAxZvgsKN+JuENXsST7F89Tck2iTcQIT8g5rwWC
P9Vt74yboe2kDT531w8+egz7nAmRBKNM751U/95P9t88EDacDI/Z2OwnuFQHCPDF
llYOUI+SpLJ6/vURRbHSnnn8a/XG+nzedGH5JGqEJNQsz+xT2axM0/W/CRknmGaJ
kda/AoGANWrLCz708y7VYgAtW2Uf1DPOIYMdvo6fxIB5i9ZfISgcJ/bbCUkFrhoH
+vq/5CIWxCPp0f85R4qxxQ5ihxJ0YDQT9Jpx4TMss4PSavPaBH3RXow5Ohe+bYoQ
NE5OgEXk2wVfZczCZpigBKbKZHNYcelXtTt/nP3rsCuGcM4h53s=
-----END RSA PRIVATE KEY-----
=end
